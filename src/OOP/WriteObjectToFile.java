package OOP;

import java.io.*;
import java.nio.Buffer;
import java.util.Scanner;

public class WriteObjectToFile {

    private void createFile(String fileName){
        Scanner sc = new Scanner(System.in);
        String pathFile = "D:\\" + fileName;
        File file = new File(pathFile);
        try {
            if(file.createNewFile()){
                System.out.println("Tạo file thành công !");
            }
        } catch (IOException e) {
            System.out.println("File đã tồn tại !");
            e.printStackTrace();
        }
    }
    public void deleteFile(){
        Scanner sc= new Scanner(System.in);
        System.out.println("Nhập tên file muốn xóa : ");
        String fileName = sc.nextLine();
        String path = "D:\\" + fileName;
        File file = new File(path);
        if(file.delete()){
            System.out.println("Xóa file thành công !");
        }else{
            System.out.println("Xóa file thất bại !");
        }
    }

    public void writeObjectToFile(){
        Scanner sc= new Scanner(System.in);
        System.out.println("Nhập tên file muốn ghi : ");
        String fileName = sc.nextLine();
        createFile(fileName);
        String path = "D:\\" + fileName;
        EnterInputFormKeyBoard input = new EnterInputFormKeyBoard();
        String infoStudent = input.setStudent().toString();


        Writer file=null;
        BufferedWriter bufferedWriter =null;
        try {
             file = new FileWriter(path,true);
             bufferedWriter = new BufferedWriter(file);
             bufferedWriter.newLine();
             bufferedWriter.write(infoStudent);

            System.out.println("Ghi dữ liệu vào file thành công !");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(bufferedWriter !=null){
                try {
                    bufferedWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
    public void readFile(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập tên file muốn đọc :");
        String fileName = sc.nextLine();
        String path = "D:\\" +fileName;
        FileReader fr = null;
        BufferedReader br = null;
        String st = "";
        try {
            fr = new FileReader(path);
            br = new BufferedReader(fr);

            while(st != null){
                st= br.readLine();
                System.out.println(st);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
