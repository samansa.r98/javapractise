package OOP;

import java.util.Date;

public abstract class People {

    private String firstName;
    private String lastName;
    private Date dateOfBirth;
    private String birthday;
    private String placetoLive;

    public People() {
    }

    public People(String firstName, String lastName, Date dateOfBirth, String placetoLive) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.placetoLive = placetoLive;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPlacetoLive() {
        return placetoLive;
    }

    public void setPlacetoLive(String placetoLive) {
        this.placetoLive = placetoLive;
    }

    public abstract String toString();
}
