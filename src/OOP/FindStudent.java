package OOP;

import java.util.List;

public class FindStudent {
    public void findStudent(List<Student> students, String name){
        List<Student> kq = null;
        int count = 0;
        for(Student student : students){
            if(student.getFirstName().toLowerCase().contains(name.toLowerCase())){
                kq.add(student);
                count++;
            }
        }
        if(count==0){
            System.out.println("Không có kết quả ");
        }else{
            for(Student student : kq){
                System.out.println(student);
            }
        }
    }
}
