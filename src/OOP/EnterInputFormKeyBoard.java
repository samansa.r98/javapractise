package OOP;

import java.text.*;
import java.util.Date;
import java.util.Scanner;

public class EnterInputFormKeyBoard {

    public Subject setSubject(){
        Scanner sc = new Scanner(System.in);

        System.out.println("Mời nhập thông tin của môn học !");
        System.out.println("Nhập tên môn học");
        String subjectName = sc.nextLine();
        System.out.println("Mời nhập ngày bắt dầu theo format dd-MM-yyyy");
        String startDate  = sc.nextLine();
        System.out.println("Mời nhập ngày kết thúc theo format dd-MM-yyyy");
        String endDate = sc.nextLine();

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyy");
        Date stDate =null;
        Date eDate=null;
        try {
             stDate = dateFormat.parse(startDate);
             eDate = dateFormat.parse(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new Subject(subjectName,stDate,eDate);
    }
    public Student setStudent(){
        Scanner sc =new Scanner(System.in);
        System.out.println(" Mời nhập tên đầu cửa sinh viên : ");
        String fName = sc.nextLine();
        System.out.println("Mời nhập tên sau của sinh viên : ");
        String lName = sc.nextLine();
        System.out.println("Mời nhập ngày sinh của sinh viên theo format dd-MM-yyyy");
        String birthdayString = sc.nextLine();
        System.out.println("Mời nhập nơi ỏ của sinh viên : ");
        String placeToLive = sc.nextLine();

        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date birthday=null;
        try {
            birthday = df.parse(birthdayString);
        }catch (ParseException ex){
            ex.getStackTrace();
        }

        Student student = new Student();
        student.setFirstName(fName);
        student.setLastName(lName);
        student.setDateOfBirth(birthday);
        student.setPlacetoLive(placeToLive);

        return student;
    }
    public Teacher setTeacher(){
        Scanner sc =new Scanner(System.in);
        System.out.println(" Mời nhập tên đầu cửa giáo viên : ");
        String fName = sc.nextLine();
        System.out.println("Mời nhập tên sau của giáo viên : ");
        String lName = sc.nextLine();
        System.out.println("Mời nhập ngày sinh của giáo viên theo format dd-MM-yyyy");
        String birthdayString = sc.nextLine();
        System.out.println("Mời nhập nơi ỏ của giáo viên : ");
        String placeToLive = sc.nextLine();

        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date birthday=null;
        try {
            birthday = df.parse(birthdayString);
        }catch (ParseException ex){
            ex.getStackTrace();
        }

        Teacher teacher = new Teacher();
        teacher.setFirstName(fName);
        teacher.setLastName(lName);
        teacher.setDateOfBirth(birthday);
        teacher.setPlacetoLive(placeToLive);

        return teacher;
    }
}
