package OOP;

import java.util.*;

public class SortStudentAndTeacher {

    public List<People> listPeople = null;

    public List<People> initObject(){
        listPeople = new ArrayList<People>();

        People st1 = new Student();
        st1.setFirstName("Vương");
        st1.setLastName("Huy");
        st1.setDateOfBirth(new Date());
        st1.setPlacetoLive("Bắc Ninh");

        listPeople.add(st1);

        People st2 = new Student();
        st2.setFirstName("Giang");
        st2.setLastName("Kim");
        st2.setDateOfBirth(new Date());
        st2.setPlacetoLive("Lương Tài");

        listPeople.add(st2);

        People st3 = new Student();
        st1.setFirstName("Khánh");
        st1.setLastName("Bá");
        st1.setDateOfBirth(new Date());
        st1.setPlacetoLive("Yên Phong");

        listPeople.add(st3);

        People te1 = new Teacher();
        st1.setFirstName("Teacher1");
        st1.setLastName("Bá");
        st1.setDateOfBirth(new Date());
        st1.setPlacetoLive("Yên Phong");

        People te2 = new Teacher();
        st1.setFirstName("Teacher2");
        st1.setLastName("Bá");
        st1.setDateOfBirth(new Date());
        st1.setPlacetoLive("Bắc Ninh");



        return listPeople;
    }

    public void sortByFirstName(List<People> listPeople){

        Comparator<People> comparatorByFirstName = new Comparator<People>() {
            @Override
            public int compare(People o1, People o2) {
                // sort people's first name by DES
                return o2.getFirstName().compareTo(o1.getFirstName());
            }
        };
        System.out.println("Trước khi sắp xếp : " + listPeople);
        try{

            Collections.sort(listPeople,comparatorByFirstName);
            System.out.println("Sau khi sắp xếp : " + listPeople);
        }catch(NullPointerException ex){
            System.out.println("Có xuất hiện phần từu null, không thể so sánh");
        }

    }

    public void sortByFullName(List<People> listPeople){

        System.out.println("Trước khi sắp xếp : " + listPeople);
        Collections.sort(listPeople, new Comparator<People>() {
            @Override
            public int compare(People o1, People o2) {
                return (o2.getFirstName() + o2.getLastName()).
                        compareTo((o1.getFirstName() + "" + o1.getFirstName()));
            }
        });
        System.out.println("Sau khi sắp xếp : " + listPeople);

    }

    public void sortNumber(List<Integer> num){

        Comparator<Integer> numberCompare = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        };
        System.out.println("Trước khi sắp xếp : "+ num);
        Collections.sort(num,numberCompare);
        System.out.println("Sau khi sắp xếp : "+ num);
    }

}
