package Annotation.JsonNameBase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TestJsonCovertAnnotation {
    public static void main(String[] args) throws IllegalAccessException {

        Supermen sup1 = new Supermen();
        sup1.setName("Vuong");
        sup1.setBirthday(new Date());
        sup1.setAge(23);

        List<String> skills = new ArrayList<>();
        skills.add("Spring boot");
        skills.add("RMI java");
        skills.add("React JS");

        sup1.setSkills(skills);

        System.out.println(JsonNameProcess.toJson(sup1));
    }
}
