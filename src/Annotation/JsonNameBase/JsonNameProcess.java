package Annotation.JsonNameBase;

import java.lang.reflect.Field;
import java.util.Optional;

public class JsonNameProcess {

    public static String toJson(Object object) throws IllegalAccessException {
        StringBuffer sb = new  StringBuffer();

        Class<?> clazz = object.getClass();
        JsonName jsonClassName = clazz.getDeclaredAnnotation(JsonName.class);

        sb.append("{\n")
                .append("\t\"")
                //nếu jsonClassName != null thì lấy giá trị của nó, orElse nếu không thì lấy tên của class luôn
                .append(Optional.ofNullable(jsonClassName).map(JsonName::value).orElse(clazz.getSimpleName()))
                .append("\" : {\n");

        Field field[] = clazz.getDeclaredFields();
        for(int i = 0 ; i < field.length; i ++){
            field[i].setAccessible(true);
            JsonName jsonNameField = field[i].getDeclaredAnnotation(JsonName.class);
            sb.append("\t\t\"")
                    .append(Optional.ofNullable(jsonNameField).map(JsonName::value).orElse(field[i].getName()))
                    .append("\":")
                    .append(field[i].getType() == String.class || !field[i].getType().isPrimitive() ? "\"" : "")
                    .append(field[i].get(object))
                    .append(field[i].getType() == String.class || !field[i].getType().isPrimitive() ? "\"" : "")
                    .append(i !=field.length-1 ? ",\n" : "\n");

        }
        sb.append("\t}\n")
                .append("}");


        return sb.toString();
    }
}
