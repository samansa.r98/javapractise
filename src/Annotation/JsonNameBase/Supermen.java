package Annotation.JsonNameBase;

import java.util.Date;
import java.util.List;

@JsonName("super_man")
public class Supermen {

    @JsonName(value = "name_of_superman")
    private String name;

    private Date birthday;

    private int age;

    @JsonName(value = "superman_skill")
    private List<String> skills;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<String> getSkills() {
        return skills;
    }

    public void setSkills(List<String> skills) {
        this.skills = skills;
    }
}
