package exercise;

import java.lang.reflect.Array;
import java.util.Arrays;

public class BubbleSort {
    public void bubbleSortByFor( int[] a){
        int temp = 0;
        System.out.println("mảng trước khi sắp xếp : " + Arrays.toString(a));
        for(int i =0; i<a.length -1 ; i++){
            for(int j=0; j<a.length-1-i;j++){
                if(a[j] > a[j+1]){
                    temp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = temp;
                }
            }
        }
        System.out.println("mảng sau khi sắp xếp : " + Arrays.toString(a));

    }
    public void bubbleSortByWhile(int[] a){
        int temp = 0;
        int i = 0,j=0;
        System.out.println("mảng trước khi sắp xếp : " + Arrays.toString(a));
        while(i<a.length-1){
            while (j<a.length-1-i){
                if(a[j] > a[j+1]) {
                    temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                }
                j++;
            }
            i++;
        }
        System.out.println("mảng trước khi sắp xếp : " + Arrays.toString(a));
    }
}
