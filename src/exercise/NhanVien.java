package exercise;

public class NhanVien {
    public static void main(String[] args) {

        TaxCalculator tc= income -> {
            if(income < 20000){
                return 0;
            }else if(income<40000){
                return (double)(Math.round((income*0.1)*100.0)/100.0);
            }else if(income <60000){
                return (double)(Math.round((income*0.2)*100.0)/100.0);
            }else {
                return (double)(Math.round((income*0.3)*100.0)/100.0);
            }
        };
        System.out.printf("Thue cua nhan vien là : %.2f",tc.taxCal(29991));

    }

    @FunctionalInterface
    interface TaxCalculator {
        double taxCal(int income);
    }
}
