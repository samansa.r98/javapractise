package exercise;

public class CalAverInArray {


    public CalAverInArray(){}

    public float CalAver(int a[]){
        float aver=0;
        int sum = 0;
        for (int i = 0; i <a.length; i++){
            sum += a[i];
        }
        aver = aver + sum/a.length;
        return aver;
    }
    public float CalAverEven(int a[]){
        int count= 0;
        float aver=0;
        int sum = 0;
        for (int i = 0; i <a.length; i++){
            if(a[i]%2==0){
                sum += a[i];
                count++;
            }
        }
        aver = aver + sum/count;
        return aver;
    }
}
