package LearningMultiThread.TestNotifyVsNotifyAll;

public class Student2 extends Thread{
    private Student student;

    public Student2(Student student){
        this.student = student;
    }

    @Override
    public void run() {
        synchronized (this.student){
            System.out.println(" thread " + getName() + " vao waiting set");
            try {
                this.student.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(" thread " + getName() + " thoat waiting set");
        }
    }
}
