package LearningMultiThread.WaitNotify;

public class Customer {
    private int balace=1000;

    public synchronized void withDraw(int amount){
        System.out.println("Đang thực hiện giao dịch rút tiền ..." + amount);
        while(balace<amount){
            System.out.println("------------------------------");
            System.out.println("Tài khoản không đủ tiền để rút ");
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println(e.toString());
            }
        }
        balace = balace-amount;
        System.out.println("Rút tiền thành công ! , Tài khoản còn lại :  " + balace);
    }
    public synchronized void deposit(int amount){
        System.out.println("-----------------------");
        System.out.println("Đang thực hiện giao dịch nạp tiền..." + amount);
        balace = balace+ amount;
        System.out.println("Tài khoản sau khi nạp tiền là : " + balace);
        notifyAll();
    }
}
