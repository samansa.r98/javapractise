package LearningMultiThread.WaitNotify;

public class GiaoDich {
    public static void main(String[] args) {
        final Customer ct = new Customer();

        Thread th1 = new Thread(){
            @Override
            public void run() {
                ct.withDraw(2000);
            }
        };
        th1.start();
        Thread th2 = new Thread(){
            @Override
            public void run() {
                ct.deposit(500);
                try{
                    Thread.sleep(2000);
                }catch (InterruptedException ex){
                    System.out.println(ex.toString());
                }
                ct.deposit(2000);

            }
        };
        th2.start();
    }
}
