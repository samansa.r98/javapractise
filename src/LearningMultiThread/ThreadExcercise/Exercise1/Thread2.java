package LearningMultiThread.ThreadExcercise.Exercise1;

public class Thread2 implements Runnable {

    @Override
    public void run() {
        System.out.println("thread 2 is running !");
        removeLampOff();
    }

    public void removeLampOff(){
        Store store = Store.getInstance();
        Trash trash = Trash.getInstance();

        while(true){
            if(!store.isEmpty()){
                for(int i = 0; i < store.size(); i++){

                    if(store.get(i).getStatus() == Status.OFF){

                        trash.add(store.get(i));
                        System.out.println("thread 2 just ADD a Lamp to Trash : " + store.get(i).toString());

                        System.out.println("thread 2 just REMOVE a Lamp from Store : " + store.get(i).toString());
                        store.remove(store.get(i));

                        if( i != 0){
                            i--;
                        }
                    }
                }
                //SHOW ALL LAMP IN STORE
                System.out.println("LIST LAMP IN STORE IS : ");
                for(int i = 0; i< store.size(); i++){
                    System.out.println(store.get(i).toString());
                }
                try {
                    System.out.println("Thread 2 is SLEEP");
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }else{
                try {
                    System.out.println("Thread 1 is SLEEP");
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }
}
