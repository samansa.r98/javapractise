package LearningMultiThread.ThreadExcercise.Exercise2;

public class Thread3 implements Runnable{

    @Override
    public void run() {

        System.out.println("Thread 3 is running !");
        checkLampInTrash();
    }
    public void checkLampInTrash(){
        Trash trash = Trash.getInstance();
        Store store = Store.getInstance();

        while(true){

            if(!trash.isEmpty()){
                for(int i = 0; i < trash.size(); i++){
                    if(trash.get(i).getStatus() == Status.REPAIR){
                        store.add(trash.get(i));
                        System.out.println(" Thread 3 just REVERT Lamp have status Repair from Trash to Store " + trash.get(i).toString());
                        System.out.println(" THread 3 just DELETE Lamp have status Repair in Trash " + trash.get(i).toString());
                        trash.remove(trash.get(i));
                        if(i !=0){
                            i--;
                        }
                    }else{
                        int rd = Util.randomInt(0,1);
                        if(rd == 0){
                            System.out.println(" THread 3 just REMOVE Lamp have Status OFF and random equal 0 " + trash.get(i));
                            trash.remove(trash.get(i));
                            if(i !=0 ){
                                i--;
                            }
                        }else{
                           trash.get(i).setStatus(Status.REPAIR);
                            System.out.println(" Thread 3 just CHANGE Status into REPAIR for Lamp have Status OFF and random equal 1 " + trash.get(i));
                        }
                    }
                }
                try {
                    System.out.println("THread 3 is Sleep");
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
