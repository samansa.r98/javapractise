package LearningMultiThread.ThreadExcercise.Exercise2;

import java.util.concurrent.CopyOnWriteArrayList;

public class Store extends CopyOnWriteArrayList<Lamp> {

    private static Store instance;

    public static Store getInstance(){
        if(instance == null) {
            instance = new Store();
        }
        return instance;
    }

}
