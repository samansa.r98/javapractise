package LearningMultiThread.ThreadExcercise.Exercise2;

enum Status {
    ON,OFF,REPAIR
}

public class Lamp {
    private Status status;
    private int index;
    private static int count;

    public Lamp(Status status){
        this.status = status;
        count=count+1;
        index = count;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "Lamp have index: " + this.index + " Have status : " + this.status;
    }
}
