package FresherPractise.IOException;

public class MainException {
    public static void main(String[] args) {
        try {
            throw new RuntimeException("Throw runtime exception !");
        }catch (RuntimeException e){
            System.out.println("Catch runtime exception ");
        }catch (Exception e){
            System.out.println("Catch Exception");
        }finally {
            System.out.println(" finnally runner");
        }
    }
}
