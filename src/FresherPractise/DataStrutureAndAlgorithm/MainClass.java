package FresherPractise.DataStrutureAndAlgorithm;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class MainClass {
    public static void main(String[] args) {

        // Run STructure and algorithm

        int[] a = {-2,11,-4,13,-5,2};
        System.out.println("Simple Solution : " + FindMaxOfSubset.simpleSolution(a));
        System.out.println("More Quick Solution : " + FindMaxOfSubset.QuickAlgorithm(a));


        // Run exercise
        Double b = 1.0;
        Integer c = 1;
        System.out.println(TestExcercise.isEqual(b,c));

        int[] removeDup = {2,3,4,3,1,7,2,8,8};
        int[] rs = TestExcercise.removeDuplicateElement(removeDup);
        Arrays.stream(rs).forEach(n -> System.out.println(n));

        List<Integer> numbers = Arrays.asList(7, 2, 5, 4, 2, 1);

            long count = 0;
            for (Integer number : numbers) {
                if (number % 2 == 0) {
                    count++;
                }
            }
            System.out.printf("There are %d elements that are even", count);

        long count1 = numbers.stream().filter(num -> num % 2 == 0).count();
        System.out.printf("There are %d elements that are even", count1);
    }
}
