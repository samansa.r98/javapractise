package FresherPractise.DataStrutureAndAlgorithm;

import java.util.Arrays;
import java.util.stream.IntStream;

public class TestExcercise {
    public static boolean isEqual(Double a, Integer b){
        double a1 = a;
        int b1 = b;
        if (a1 == b1){
            return true;
        }
        return false;
    }
    // Seft thinking
    public static int[] removeDuplicate(int[] a){
        int[] b = null;
        for (int i = 0; i < a.length - 1; i++){
            for (int j = i+1; j < a.length; j++){
                if(a[i] == a[j]){
                    int indexRemove = j;
                    // chuwa loai bo duoc phan tu trung
                    b = IntStream.range(0,a.length)
                            .filter(k -> k != indexRemove)
                            .map(k -> a[k])
                            .toArray();
                    System.out.println(  "phan tu thu" + j + " trung voi phan tu thu " +
                            i + " co value : " + a[j]);
                }
            }
        }
        return b;
    }
    // second way for removing duplicate element
    // first sort array, then remove

    public static int[] removeDuplicateElement(int[] a){
        int j = 0;
        int[] temp = new int[a.length];
        Arrays.sort(a);
        for (int i = 0;i<a.length-1;i++){
            if(a[i] != a[i+1]){
                temp[j++] = a[i];
            }
        }
        temp[j++] = a[a.length-1];
        return temp;
    }
}
