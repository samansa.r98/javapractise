package FresherPractise.DataStrutureAndAlgorithm;

public class FindMaxOfSubset {

    public static int simpleSolution(int[] a){
//        int[] b = null;
        int maxSum = 0;
        for(int i = 0; i < a.length; i++){
            for(int j = i; j< a.length; j++){
                int sum = 0;
                for (int k = i; k <= j; k++){
                    sum += a[k];
                }
                if(sum > maxSum){
                    maxSum = sum;
                }
            }
        }
        return maxSum;
    }
    public static int QuickAlgorithm(int[] a ){
        int maxSum = 0;
        for (int i = 0 ; i < a.length ; i++){
            int sum = 0;
            for(int j = i; j < a.length; j++){
                sum += a[j];
                if (sum > maxSum){
                    maxSum = sum;
                }
            }
        }
        return maxSum;
    }

}
