package FresherPractise.OOP;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class OOPMain {

    public static void main(String[] args) {
        Animal animal = new Animal();
        System.out.println(animal.witch);

        Animal animal1 = new Cat();
        ((Cat)animal1).showDialog();
        animal1.speaking();
        System.out.println("name of animal is :" + animal1.name);

        Cat cat = new Cat();

        cat.setName("Tom");
        cat.showDialog();
        System.out.println("name of animal is :" + animal1.name);

        //Cat cat1 = (Cat)new Animal(); // throw class cast exception
        //cat1.showDialog();


        // tesst iterator
        List<String> test = new ArrayList<>();

        test.add("Vuong");
        test.add("Cong");
        test.add("Lien");
        test.add("Bang");

        for(int i = 0; i<2 ; i++){
            System.out.println(" CHay lan " + i);
            for(int j = 0; j<test.size();j++){
                System.out.println(test.get(j));
            }
        }
        System.out.println(" CHay iterator : ");
        Iterator<String> iter = test.iterator();

        for(int i= 0; i<2;i++){
            System.out.println("chay lan " + i);
            while(iter.hasNext()){
                System.out.println(iter.next());
            }
        }

        // TEST REMOVE IN FOREACH ==> run error by in foreach, element cant modify
        for(String i: test){
            if(i.equals("Vuong")){
                System.out.println(i);
                test.remove(i);
                System.out.println(" Remove xong");
            }
        }
        for(int i = 0; i< test.size(); i++){
            System.out.println(test.get(i));
        }

        // test polymorphism , downcasting

        Animal animal2 = new Cat();
        if(animal2 instanceof Cat){
            Cat cat1 = (Cat) animal2;
            cat1.meomeo();
        }
    }
}
