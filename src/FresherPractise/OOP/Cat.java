package FresherPractise.OOP;

public class Cat extends Animal{
    //public String name = "Tom";

    @Override
    public void speaking(){
        System.out.println("Cat speaking");
    }

    public void setName(String name){
        this.name = name;
    }
    public void showDialog(){
        System.out.println(" this is dialog from Cat class " + this.name);
    }

    public void meomeo(){
        System.out.println("meo meo");
    }
}
