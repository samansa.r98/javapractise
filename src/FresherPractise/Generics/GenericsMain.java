package FresherPractise.Generics;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

public class GenericsMain {

    public static void main(String[] args) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {

        System.out.println(Operators.add(1.3,2.5));

        // bai 1
        List test1 = Arrays.asList(1,3,4,6,2,3,6);
        System.out.println(GenericsExercise.countOddNumber(test1,new OddImplementation()));

        // bai 2
        // bai 3
        // bai 4
        // bai 5
        Pair<Integer,String> pair = new Pair<>(2,"abc");
        // Pair<Integer,String> pair = new Pair<>(Integer.class,String.class);
        Integer key = pair.getKey();
        String value = pair.getValue();

    }

}
