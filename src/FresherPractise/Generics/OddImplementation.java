package FresherPractise.Generics;

public class OddImplementation implements UnaryPredicate<Integer>{

    @Override
    public boolean checkOddNumber(Integer object) {

        if (object % 2 != 0){
            return true;
        }
        return false;
    }
}
