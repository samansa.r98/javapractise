package FresherPractise.Generics;

public interface UnaryPredicate<T> {
    public boolean checkOddNumber(T object);
}
