package FresherPractise.Generics;

public class Operators {

    public static int add(int a, int b){
        System.out.println(" traditional add have used");
        return a + b;
    }

    public static <T extends Number> double add(T a, T b){
        System.out.println(" generics add have used");
        return a.doubleValue() + b.doubleValue();
    }


}
